<?php

/*
 * CREATE DATABASE IF NOT EXISTS `witch_hunt` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
 *
Table schema

CREATE TABLE `tbl_leaderboard` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 `score` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
*/

function query($db, $query, $params = []) {
	foreach ($params as $param => $value) {
		$value = mysqli_real_escape_string($db, $value);
		$query = str_replace($param, $value, $query);
	}
	return mysqli_query($db, $query);
}

$db = mysqli_connect('127.0.0.1', 'root', '', 'witch_hunt');
mysqli_set_charset($db, "utf8");

if (isset($_GET['name']) && isset($_GET['score'])) {
	query($db, "INSERT INTO tbl_leaderboard (name, score) VALUES (':name', :score)", [':name' => $_GET['name'], ':score' => $_GET['score']]);
} else {
	$result = [];
	$leaders = query($db, "SELECT * FROM tbl_leaderboard ORDER BY score DESC LIMIT 10");
    $place = 0;
	while ($leader = mysqli_fetch_array($leaders)) {
        $place++;
		$result[] = [
		    'place' => $place,
			'name' => $leader['name'],
			'score' => $leader['score']
		];
	}
	echo json_encode($result);
}

if (isset($leaders) && is_resource($leaders)) { free($leaders); }
if (isset($db) && is_resource($db)) { mysqli_close($db); }