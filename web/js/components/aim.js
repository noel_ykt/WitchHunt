var Aim = function (game) {
    Phaser.Sprite.call(this, game, game.world.centerX, game.world.centerY, 'aim');

    game.physics.enable(this, Phaser.Physics.ARCADE);
    this.anchor.set(.5);
    this.body.collideWorldBounds = true;

    // game.input.addMoveCallback(this.move, this);

    game.add.existing(this);
    return this;
};
Aim.prototype = Object.create(Phaser.Sprite.prototype);
Aim.prototype.constructor = Aim;

Aim.prototype.move = function (pointer, x, y, isDown) {
    this.x = x;
    this.y = y;
};
Aim.prototype.update = function () {
    this.game.physics.arcade.moveToPointer(this, 800, Phaser.Input.activePointer, 250);
    if (Phaser.Rectangle.contains(this.body, this.game.input.x, this.game.input.y)) {
        this.body.velocity.setTo(0, 0);
    }
};