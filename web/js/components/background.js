Background = function (game) {
    Phaser.Group.call(this, game, game.world);

    this.back = this.game.add.sprite(0, 0, 'background');
    this.tavern = this.game.add.sprite(200, 170, 'tavern');
    this.cemetry = this.game.add.sprite(0, 0, 'cemetry');
    this.tavern_inside = this.game.add.sprite(0, 0, 'tavern_inside');

    this.setBackground("tavern_outside");
}
Background.prototype = Object.create(Phaser.Group.prototype);
Background.prototype.constructor = Background;

Background.prototype.setBackground = function(place) {
    this.back.visible = false;
    this.tavern.visible = false;
    this.tavern_inside.visible = false;
    this.cemetry.visible = false;
    switch (place) {
        case "tavern_inside":
            this.tavern_inside.visible = true;
            break;
        case "cemetry":
            this.cemetry.visible = true;
            break;
        case "tavern_outside":
            this.back.visible = true;
            this.tavern.visible = true;
            break;
        case "blank":
            this.back.visible = true;
            break;
        default:
            this.back.visible = true;
    }
}
