var Boss = function (game) {
    Phaser.Sprite.call(this, game, -1000, -1000, 'boss_right');

    this.animations.add('boss_right', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
    this.animations.add('boss_left', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
    this.animations.play('boss_right');
    this.game.global.signals.shot.add(this.shot, this);
    this.health = 3;

    game.add.existing(this);
    return this;
};

Boss.prototype = Object.create(Phaser.Sprite.prototype);
Boss.prototype.constructor = Boss;

Boss.prototype.update = function () {
    switch (this.status) {
        case 'live':
            if (this.position.equals(this.wayPoint)) {
                this.setWayPoint();
            }
            this.move();
            break;
        default:
            break;
    }
};

Boss.prototype.move = function () {
    if (Phaser.Point.distance(this.wayPoint, this.position) < this.speed) {
        this.position.copyFrom(this.wayPoint);
    } else {
        this.position.add(Math.cos(this.direction) * this.speed, Math.sin(this.direction) * this.speed);
    }
};

Boss.prototype.reset = function (speed) {
    this.x = this.game.world.centerX - 100;
    this.y = this.game.world.centerY - 150;
    this.speed = speed;
    this.status = 'live';
    this.wayPoint = new Phaser.Point(0, 0);
    this.setWayPoint();
    this.updateTexture();
};

Boss.prototype.kill = function () {
    this.game.global.signals.hit.dispatch();
    this.status = 'dead';
    this.game.time.events.add(500, function () {
        this.game.global.signals.roundEnd.dispatch();
    }, this);
    this.updateTexture();
};

Boss.prototype.shot = function (point) {
    if (this.position.x <= point.x && point.x <= this.position.x + this.width &&
            this.position.y <= point.y && point.y <= this.position.y + this.height) {
        this.health--;
        this.game.global.hits++;
        this.game.global.rounds[0].killed += 3;
    } else {
        this.game.global.signals.missed.dispatch();
    }
    this.game.global.signals.endShot.dispatch();
};

Boss.prototype.setWayPoint = function (x, y) {
    x = x || this.game.rnd.integerInRange(100, this.game.width - 100);
    y = y || this.game.rnd.integerInRange(50, this.game.height - 300);
    this.wayPoint.set(x, y);
    this.direction = Phaser.Point.angle(this.wayPoint, this.position);
    this.updateTexture();
};

Boss.prototype.updateTexture = function () {
    switch (this.status) {
        case 'ready':
        case 'live':
            var dg = this.direction * (180 / Math.PI);
            if (90 > dg && dg >= -90) {
                this.animations.play('boss_right');
            } else {
                this.animations.play('boss_left');
            }
            break;
        case 'dead':
            this.animations.play('die');
            break;
        default:
            break;
    }
};