var Dog = function (game) {
    this.start_x = 50;
    this.start_y = 410;
    Phaser.Sprite.call(this, game, this.start_x, this.start_y, 'dog');
    this.after_bushes_position_x = 400;
    this.after_bushes_position_y = 420;
    this.down = false;
    this.status = 'start';
    this.animations.add('walk', [0, 1, 2, 0, 1, 2], 4, false);
    this.animations.add('sniff', [3, 4, 3, 4], 4, false);
    this.animations.add('stay', [6], 1, false);
    this.animations.add('jump', [7, 8], 5, false);
    this.animations.add('hidden', [12], 1, false);
    this.animations.add('show_one_duck', [5], 1, false);
    this.animations.add('show_two_duck', [11], 1, false);
    this.animations.add('laugh', [9, 10], 4, true);
    this.scale.set(2);

    this.laughAudio = game.add.audio('dogLaugh');

    game.global.signals.levelStart.add(this.walk, this);
    game.global.signals.roundEnd.add(this.roundEnd, this);

    game.add.existing(this);
    return this;
};
Dog.prototype = Object.create(Phaser.Sprite.prototype);
Dog.prototype.constructor = Dog;

Dog.prototype.update = function () {
    this.move();
};

Dog.prototype.roundEnd = function () {
    var currentRound = this.game.global.rounds[this.game.global.currentRound];
    var killedWitches = currentRound ? currentRound.killed : 0;
    if (killedWitches > 0) {
        this.showWitches(killedWitches);
    } else {
        this.laugh();
    }
};

Dog.prototype.walk = function () {
    this.status = 'walk';
    this.x = this.start_x;
    this.y = this.start_y;
    this.animations.play('walk').onComplete.addOnce(function () {
        this.status = 'sniff';
        this.animations.play('sniff').onComplete.addOnce(function () {
            this.status = 'stay';
            this.animations.play('stay').onComplete.addOnce(function () {
                this.status = 'jump';
                this.animations.play('jump').onComplete.addOnce(function () {
                    this.hide();
                    this.game.global.signals.roundStart.dispatch();
                }, this);
            }, this);
        }, this);
    }, this);
};

Dog.prototype.showWitches = function (countWitches) {
    this.down = false;
    this.status = 'look_out';
    this.x = this.game.global.witchFallPosition - 30;
    this.y = this.after_bushes_position_y;
    if (countWitches == 1) {
        this.animations.play('show_one_duck');
    } else {
        this.animations.play('show_two_duck');
    }
    var tweenUp = this.game.add.tween(this).to({y: 350}, 2000, Phaser.Easing.Cubic.Out, true, 0, 0, false);
    tweenUp.onComplete.addOnce(function () {
        var tweenDown = this.game.add.tween(this).to({y: 450}, 1000, Phaser.Easing.Cubic.Out, true, 0, 0, false);
        tweenDown.onComplete.addOnce(function () {
            this.hide();
            this.game.global.signals.dogEnd.dispatch();
        }, this);
    }, this);
};

Dog.prototype.laugh = function () {
    this.down = false;
    this.status = 'laugh';
    this.x = this.after_bushes_position_x;
    this.y = this.after_bushes_position_y;
    this.animations.play('laugh');
    this.laughAudio.play();
    var tweenUp = this.game.add.tween(this).to({y: 350}, 2000, Phaser.Easing.Cubic.Out, true, 0, 0, false);
    tweenUp.onComplete.addOnce(function () {
        var tweenDown = this.game.add.tween(this).to({y: 450}, 1000, Phaser.Easing.Cubic.Out, true, 0, 0, false);
        tweenDown.onComplete.addOnce(function () {
            this.hide();
            this.game.global.signals.dogEnd.dispatch();
        }, this);
    }, this);
};

Dog.prototype.hide = function () {
    this.animations.play('hidden');
    this.status = 'ready';
};

Dog.prototype.move = function () {
    if (this.status == 'walk' || this.status == 'jump') {
        if (this.status == 'jump') {
            this.y -= 2;
        }
        this.x += 1;
    }
};
