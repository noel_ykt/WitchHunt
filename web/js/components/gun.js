var Gun = function(x, y, game) {
    Phaser.Sprite.call(this, game, x, y, 'shotgun');
    this.anchor.set(.5);
    this.status = 'ready';
    this.animations.add('idle', [0], 1, true);
    this.animations.add('shoot', [0, 1, 1, 2, 2, 2, 0], 15, false);
    this.animations.add('reload', [0, 0, 0, 0, 0, 0, 0], 5, false);
    this.aim = new Aim(game);

    this.shotgunAudio = game.add.audio('shotgun');
    this.shotgunAudio.volume = .5;

    game.add.existing(this);
    return this;
};

Gun.prototype = Object.create(Phaser.Sprite.prototype);
Gun.prototype.constructor = Gun;

Gun.prototype.update = function () {
    switch (this.status) {
        case 'ready':
            break;
        case 'shoot':
            this.status = 'busy';
            this.animations.play('shoot');
            this.animations.currentAnim.onComplete.addOnce(function () {
                this.status = 'ready';
            }, this);
            break;
        default:
            break;
    }
};

Gun.prototype.shoot = function () {
    if (this.status == 'ready') {
        var canShoot = this.game.global.bullets > 0;
        if (canShoot) {
            this.status = 'shoot';
            this.boom();
            this.game.global.signals.shot.dispatch(this.aim);
        }
    }
};

Gun.prototype.boom = function() {
    this.shotgunAudio.play();
}
