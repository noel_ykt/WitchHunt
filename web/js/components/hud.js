Hud = function (game, witch, dog, shotgun, background) {
    Phaser.Group.call(this, game, game.world);

    this.witch = witch;
    this.dog = dog;
    this.shotgun = shotgun;
    this.background = background;
    //hud
    var hudOffsetY = 215;
    this.roundText = game.add.text(170, 296 + hudOffsetY, "R=0", {}, this);
    this.scoreText = game.add.text(524, 325 + hudOffsetY, "000000\nSCORE", {}, this);
    this.scoreText.lineSpacing = -4;
    this.hitText = game.add.text(252, 325 + hudOffsetY, "HIT", {}, this);

    this.roundText.setStyle({font: "15px pressFont", fill: "#98f515"}, true);
    this.scoreText.setStyle({font: "17px pressFont", fill: "#98f515"}, true);
    this.hitText.setStyle({font: "17px pressFont", fill: "#fff"}, true);

    var bulletx = 168;
    var bullety = 328 + hudOffsetY;
    this.bullets = this.create(bulletx, bullety, 'bullets');

    var skullx = 320;
    var skully = 325 + hudOffsetY;
    this.skullsWhite = [];
    this.skullsRed = [];
    for (var i = 0; i < 10; i++) {
        var skull = this.create(skullx + i * 17, skully, "skull_white");
        this.skullsWhite.push(skull);
        skull = this.create(skullx + i * 17, skully, "skull_red");
        this.skullsRed.push(skull);
    }
    for (var i = 0; i < 24; i++) {
        this.create(skullx + i * 4, skully + 21, "bar_blue");
    }
    this.skullAnimate = true;

    //dialog
    this.dialogBackground = this.create(0, 500, "dialog_background");
    this.dialogBackground.visible = false;
    this.message = game.add.text(120, 300 + hudOffsetY, "", {
        font: "20px pressFont",
        fill: "#fff",
        wordWrap: true,
        wordWrapWidth: 600
    }, this);
    this.catFace = this.create(20, 510, "cat_face");
    this.catFace.visible = false;
    this.dogFace = this.create(20, 510, "dog_face");
    this.dogFace.visible = false;
    this.witchFace = this.create(20, 510, "witch_face");
    this.witchFace.visible = false;

    //events
    this.game.global.signals.missed.add(this.drawHud, this);
    this.game.global.signals.roundStart.add(function() {
        this.drawHud();
    }, this);
    this.game.global.signals.levelStart.add(function () {
        this.skullsInit();
        this.drawHud();
    }, this);
    this.game.global.signals.roundEnd.add(this.drawHud, this);
    this.game.global.signals.levelEnd.add(this.drawHud, this);

    this.game.global.signals.shot.add(function () {
        this.drawHud();
        this.messageShot();
    }, this);
    this.game.global.signals.hit.add(function () {
        this.messageHit();
        this.witchCastFlip();
    }, this);
    this.game.global.signals.missed.add(this.messageMissed, this);

    this.game.global.signals.preLevelStart.add(this.preLevelStart, this);

    //other
    this.witchIsCastFlip = false;

    //dialog text
    this.dialogs = {
        entrance:[
            {who: this.catFace, text: "Долго же мы ехали, у меня вся шерсть помялась от тесной повозки."},
            {who: this.dogFace, text: "Не говори, надо зайти в местную таверну. Мой старый знакомый хозяин там."},
            {who: this.catFace, text: "Хозяин говоришь, хехе."},
            {who: this.dogFace, text: "Брось свои шуточки, давай лучше помоги разгрузить вещи."},
        ],
        tavern1:[
            {who: this.dogFace, text: "Эй, Бьерн! Мы с товарищем устали с дороги, налейка своего вкуснейшего эля."},
            {who: this.dogFace, text: "..."},
            {who: this.dogFace, text: "... Бьерн!"},
            {who: this.catFace, text: "Что-то нет твоего хозяина."},
            {who: this.witchFace, text: "Кхе кхе... Что вы хотели? Я вас слушаю."},
            {who: this.dogFace, text: "А где хозяин?"},
            {who: this.witchFace, text: "Бьерн уехал за овесом для лошадей. Я его помощница, желаете поесть?"},
            {who: this.witchFace, text: "Сегодня у нас жареные сосиски в тесте и суп с котом."},
            {who: this.catFace, text: "Что с котом?"},
            {who: this.witchFace, text: "Сию минуту."},
            {who: this.catFace, text: "Что-то с ней не так."},
            {who: this.dogFace, text: "Да успокойся ты, все норм..."},
        ],
        tavern2:[
            {who: this.witchFace, text: "Хаха, вы ничтожные твари будет вам еда! Из вас самих!"},
            {who: this.dogFace, text: "Сигурд! Стреляй в нее."},
            {who: this.witchFace, text: "Сигурд??? Сестры! Тут у нас появился знаменитый истребитель ведьм."},
            {who: this.catFace, text: "Черт подери, она улетает через окно. Давай на улицу!"},
        ],
        level1:[
            {who: this.dogFace, text: "Чтобы стрельнуть в ведьму, тебе надо сначало взять ее на прицел, и потом нажать."},
            {who: this.catFace, text: "Чтобы я без тебя делал Ивар!"},
            {who: this.dogFace, text: "И еще, ты не забыл что у тебя только три патрона в магазине."},
            {who: this.catFace, text: "Три патрона? Как? У меня же двустволка."},
            {who: this.dogFace, text: "Да не заморачивайся по пустякам. Все! Я побежал их отвлекать."},
        ],
        level2:[
            {who: this.witchFace, text: "Паршивый кот! Ты нас не остановишь, на мое место встанут другие."},
            {who: this.catFace, text: "Тогда они примут такую же участь."},
            {who: this.dogFace, text: "Может спросим где находятся другие ведьмы?"},
            {who: this.witchFace, text: "Я вам ничего не скажу."},
        ],
        level2_eye:[
            {who: this.witchFace, text: "Ай! Мой глаз!"},
            {who: this.catFace, text: "Я тебе вырежу второй, если не скажешь как найти ваших."},
            {who: this.witchFace, text: "Хорошо хорошо, кхе кхе. Возле кладбища есть гора, там каждое полнолуние происходит шабаш."},
            {who: this.catFace, text: "Кончай ее Ивар!"},
        ],
    };

    this.drawHud();
    return this;
};

Hud.prototype = Object.create(Phaser.Group.prototype);
Hud.prototype.constructor = Hud;
Hud.prototype.update = function () {
    this.skullBlink(); // TODO: подумать, как можно реализовать по другому
};

Hud.prototype.drawHud = function () {
    var global = this.game.global;

    //draw bullets
    this.bullets.frame = global.bullets;

    //draw r=0
    this.roundText.text = "R=" + global.level.number;
    if (this.game.global.level.isBoss) {
        this.roundText.text = "FIN";
    }

    //draw score
    var s = global.score + "";
    while (s.length < 6) {
        s = "0" + s;
    }
    this.scoreText.text = s + "\n" + " SCORE";

    //draw skulls
    var j = 0, i;
    global.rounds.forEach(function (x) {
        for (i = 0; i < x.witches; i++) {
            this.skullsRed[j + i].visible = false;
        }
        for (i = 0; i < x.killed; i++) {
            this.skullsRed[j + i].visible = true;
        }
        j += x.witches;
    }, this);

    if (global.currentRound !== 0) {
        if (!this.skullsRed[global.currentRound - 1].visible) {
            this.skullsWhite[global.currentRound - 1].visible = true;
        }
    }
};

Hud.prototype.clearMessage = function () {
    this.dialogBackground.inputEnabled = false;
    this.dialogBackground.visible = false;
    this.catFace.visible = false;
    this.witchFace.visible = false;
    this.dogFace.visible = false;
    this.message.visible = false;
};

Hud.prototype.messageHit = function () {
    if (Math.random() < 0.2) {
        this.drawMessage("Попал! Хаха!", this.catFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Получай ведьма!", this.catFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Сдадим ее голову шерифу!", this.dogFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Хаха! Даже с закрытым глазом.", this.catFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Аай!", this.witchFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Вот тебе!", this.catFace);
    }
};

Hud.prototype.messageShot = function () {
};

Hud.prototype.messageMissed = function () {
    if (Math.random() < 0.2) {
        this.drawMessage("Черт черт черт!", this.catFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Сигурд, твой глаз совсем не видит?.", this.dogFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Одень повязку на другой глаз, может поможет.", this.dogFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Берегись!", this.catFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Умри умри!", this.dogFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Котик совсем слепой!", this.witchFace);
    } else if (Math.random() < 0.2) {
        this.drawMessage("Я сама ночь!", this.witchFace);
    }
};

Hud.prototype.drawMessage = function(message, face, isWait) {
    isWait = isWait || false;
    this.clearMessage();
    this.dialogBackground.visible = true;
    this.message.visible = true;
    face.visible = true;
    var color = "#fff";
    if (face == this.catFace) {
        this.name = "Сигурд";
        color = "#70519D";
    } else if (face == this.dogFace) {
        this.name = "Ивар";
        color = "#89C12E";
    } else if (face == this.witchFace) {
        this.name = this.witchAppeared ? "Ведьма" : "Девушка";
        //color = this.witchAppeared ? "#FE1E2E" : "#9E1E2E";
        color = "#FE1E2E";
    }
    this.message.setStyle({
        font: "20px pressFont",
        fill: color,
        wordWrap: true,
        wordWrapWidth: 700
    }, true);
    this.message.setText(this.name + ": " + message);



    if (isWait == false) {
        setTimeout(this.clearMessage.bind(this), 2000);
    } else {
        this.game.input.onDown.addOnce(isWait, this);
    }
};

Hud.prototype.resetFlip = function() {
    this.witchIsCastFlip = false;
    var gameCont = document.getElementById('witchhunt');
    gameCont.className = 'game';
}

Hud.prototype.preLevelStart = function () {
    //если был зазеркалена игра, возвращаем обратно
    this.resetFlip();

    if (this.game.global.level.number == 1) {
        this.showDialog(this.dialogs.entrance, 0, function(){
            this.background.setBackground("tavern_inside");
            this.showDialog(this.dialogs.tavern1, 0, function(){
                //вылетает ведьма в центр
                this.witch.reset(10);
                this.witch.status = "talk";
                this.witch.laugh();
                this.witchAppeared = true;
                this.showDialog(this.dialogs.tavern2, 0, function(){
                    //убегает
                    this.witch.flyAway();
                    setTimeout(function(){
                        this.background.setBackground("tavern_outside");
                        this.showDialog(this.dialogs.level1, 0, function(){
                            this.game.global.signals.levelStart.dispatch();
                        }.bind(this));
                    }.bind(this), 1000);
                }.bind(this));
            }.bind(this));
        }.bind(this));
    } else if (this.game.global.level.number == 2)  {
        this.witch.reset(10);
        this.witch.status = "talk";
        this.showDialog(this.dialogs.level2, 0, function(){
            console.log(this);
            console.log(this.witch);
            console.log(this.shotgun);
            this.shotgun.boom();
            this.game.plugins.screenShake.shake(10); //camera shake
            this.showDialog(this.dialogs.level2_eye, 0, function(){
                this.witch.killAnimationOnly();
                setTimeout(function(){
                    this.background.setBackground("cemetry");
                    this.game.global.signals.levelStart.dispatch();
                }.bind(this), 1000);
            }.bind(this));
        }.bind(this));
    } else {
        this.background.setBackground("blank");
        this.game.global.signals.levelStart.dispatch();
    }
};

Hud.prototype.showDialog = function (dialog, number, onDialogEnd) {
    this.clearMessage();
    if (number < dialog.length) {
        this.drawMessage(dialog[number].text, dialog[number].who, function(){
            this.showDialog(dialog, number+1, onDialogEnd);
        });
    } else {
        onDialogEnd();
    }
};

Hud.prototype.witchCastFlip = function () {
    if (!this.witchIsCastFlip) {
        if (this.dialogBackground.visible == false && Math.random() < 0.2 && this.game.global.level.number > 1) {
            this.drawMessage("Неееет!.. Ахахаха! Теперь попробуй попади!", this.witchFace);
            this.witchIsCastFlip = true;
            setTimeout(function () {
                var gameCont = document.getElementById('witchhunt');
                gameCont.className += ' game--flip';
            }, 2000);
        }
    }
};

Hud.prototype.skullBlink = function () {
    if (this.skullAnimate) {
        this.skullAnimate = false;
        this.game.time.events.add(Phaser.Timer.SECOND * 0.7, function () {
            id = this.game.global.level.isBoss ? this.game.global.rounds[0].killed : this.game.global.currentRound;
            this.skullsWhite[id].visible = !this.skullsWhite[id].visible;
            this.skullAnimate = true;
        }, this);
    }
};

Hud.prototype.skullsInit = function () {
    for (var i = 0; i < this.skullsWhite.length; i++) {
        this.skullsWhite[i].visible = true;
        this.skullsRed[i].visible = false;
    }
};
