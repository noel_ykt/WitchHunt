var BackgroundMusic = function (game) {
    this.game = game;
    this.volume = 0.15;
    this.playlist = [
        {
            key: 'bgmusic1',
            volume: 0.15
        },
        {
            key: 'bgmusic2',
            volume: 0.15
        },
        {
            key: 'bgmusic3',
            volume: 0.25
        }
    ];
    this.current = 0;

    this.music = this.game.add.audio(this.playlist[this.current].key, this.playlist[this.current].volume, false);
    this.music.onStop.add(function() { this.next() }, this);
    this.music.play();
    this.game.global.signals.toggleMusic.add(function() { this.toggle() }, this);
    this.game.global.signals.levelStart.add(function() { this.next() }, this);
    this.game.global.signals.gameOver.add(function() { this.music.stop(); }, this);
    return this;
};
BackgroundMusic.prototype = Object.create(Phaser.Sound.prototype);
BackgroundMusic.prototype.constructor = BackgroundMusic;

BackgroundMusic.prototype.toggle = function () {
    if (this.music.isPlaying) {
        this.music.pause();
    } else {
        this.music.resume();
    }
};

BackgroundMusic.prototype.next = function () {
    this.current++;
    if (this.current > this.playlist.length - 1) {
        this.current = 0;
    }
    this.music.key = this.playlist[this.current].key;
    this.music.volume = this.playlist[this.current].volume;
    this.music.play();
};