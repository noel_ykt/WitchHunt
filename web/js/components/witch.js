var Witch = function (game) {
    Phaser.Sprite.call(this, game, -1000, -1000, 'witch_green');

    this.animations.add('flyRight', [0, 1], 5, true);
    this.animations.add('flyLeft', [2, 3], 5, true);
    this.animations.add('die', [4, 5, 6], 5, false);
    this.animations.add('fall', [7, 8], 5, true);
    this.visible = true;
    this.reset();
    this.game.global.signals.shot.add(this.shot, this);
    this.game.global.signals.missed.add(function () {
        if (this.status == 'live') {
            this.laugh();
        }
    }, this);
    this.game.global.signals.roundStart.add(function () {
        this.status = 'live';
        this.laugh();
    }, this);
    this.game.global.signals.flyAway.add(this.flyAway, this);

    this.laughAudio = game.add.audio('witchLaugh');
    this.laughAudio.volume = .5;
    this.fallsAudio = game.add.audio('witchFalls');
    this.landAudio = game.add.audio('witchLands');
    this.emitter = game.add.emitter(0, 0 , 100);
    this.emitter.makeParticles('blood');
    this.emitter.gravity = 500;
    game.add.existing(this);
    return this;
};

Witch.prototype = Object.create(Phaser.Sprite.prototype);
Witch.prototype.constructor = Witch;

Witch.prototype.update = function () {
    switch (this.status) {
        case 'live':
            if (this.position.equals(this.wayPoint)) {
                this.setWayPoint();
            }
            this.move();
            break;
        case 'flyaway':
            this.move();
            break;
        case 'talk':
            this.setWayPoint(400, 200);
            this.move();
            break;
        default:
            break;
    }
};

Witch.prototype.move = function () {
    if (Phaser.Point.distance(this.wayPoint, this.position) < this.speed) {
        this.position.copyFrom(this.wayPoint);
    } else {
        this.position.add(Math.cos(this.direction) * this.speed, Math.sin(this.direction) * this.speed);
    }
};

Witch.prototype.reset = function (speed) {
    if (!this.visible){
        return;
    }
    this.x = this.game.rnd.integerInRange(50, this.game.width - 50);
    this.y = 500;
    this.speed = this.roundSpeed = speed;
    this.status = 'ready';
    this.wayPoint = new Phaser.Point(0, 0);
    this.setWayPoint();
    switch (this.game.rnd.integerInRange(0, 2)) {
    	case 0:
    		this.loadTexture('witch_red');
    		break;
    	case 1:
    		this.loadTexture('witch_green');
    		break;
    	case 2:
    		this.loadTexture('witch_blue');
    		break;
    	default:
    		this.loadTexture('witch_red');
    		break;
    }
    this.updateTexture();
};

Witch.prototype.burstBlood = function() {
    this.emitter.x = this.x + 48; //MAGIC
    this.emitter.y = this.y + 48; //MAAAGIIC!
    this.emitter.start(true, 1000, null, 50);
};

Witch.prototype.kill = function () {
    //this.game.global.signals.shot.remove(this.shot, this);
    var hits = this.game.global.hits;
    this.game.global.witchFallPosition = this.x;
    this.game.global.signals.hit.dispatch();
    this.status = 'dead';
    this.game.time.events.add(500, function () {
        this.status = 'fall';
        this.animations.play('fall');
        this.fallsAudio.play();
        var tweenDown = this.game.add.tween(this).to({y: 450}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        tweenDown.onComplete.addOnce(function () {
            this.fallsAudio.stop();
            this.landAudio.play();
            if (this.game.global.rounds[this.game.global.currentRound].witches == hits){
                this.game.global.signals.roundEnd.dispatch();
            }
        }, this);
    }, this);
    this.updateTexture();
};

Witch.prototype.killAnimationOnly = function () {
    this.game.global.witchFallPosition = this.x;
    this.status = 'dead';
    this.game.time.events.add(500, function () {
        this.status = 'fall';
        this.animations.play('fall');
        this.fallsAudio.play();
        var tweenDown = this.game.add.tween(this).to({y: 450}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        tweenDown.onComplete.addOnce(function () {
            this.fallsAudio.stop();
            this.landAudio.play();
        }, this);
    }, this);
    this.updateTexture();
};

Witch.prototype.shot = function (point) {
    if (this.status == 'live') {
        if (this.position.x <= point.x && point.x <= this.position.x + this.width &&
                this.position.y <= point.y && point.y <= this.position.y + this.height) {
            this.game.global.hits++;
            this.kill();
        } else {
            this.game.global.signals.missed.dispatch();
        }
        this.game.global.signals.endShot.dispatch();
    }
};

Witch.prototype.setWayPoint = function (x, y) {
    x = x || this.game.rnd.integerInRange(50, this.game.width - 50);
    y = y || this.game.rnd.integerInRange(50, this.game.height - 200);
    this.wayPoint.set(x, y);
    this.direction = Phaser.Point.angle(this.wayPoint, this.position);
    this.updateTexture();
};

Witch.prototype.updateTexture = function () {
    switch (this.status) {
        case 'flyaway':
        case 'ready':
        case 'live':
        case 'talk':
            var dg = this.direction * (180 / Math.PI);
            if (90 > dg && dg >= -90) {
                this.animations.play('flyRight');
            } else {
                this.animations.play('flyLeft');
            }
            break;
        case 'dead':
            this.animations.play('die');
            this.burstBlood();
            break;
        default:
            break;
    }
};

Witch.prototype.flyAway = function () {
    this.status = 'flyaway';
    var x = -1000;
    var dg = this.direction * (180 / Math.PI);
    if (90 > dg && dg >= -90) {
    	x = 1000;
    }
    this.setWayPoint(x, this.position.y);
    this.speed = 15;
    this.updateTexture();
};

Witch.prototype.laugh = function() {
    this.laughAudio.play();
}
