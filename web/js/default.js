(function () {
    var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'witchhunt', null, false, true, Phaser.Physics.ARCADE);

    game.global = {
        level: {
            number: 1, // текущий уровень
            minKilledWitches: 6, // минимальное кол-во ведьм которое надо убить за текущий уровень
            isBoss: false
        },
        currentRound: 0,
        bossLevel: 2,
        rounds: [], // тут будут объекты { witches: 1, killedWitches: 1 }
        bullets: 3, // кол-во патронов, остаток
        score: 0, // общее набранное очко
        scorePerWitch: 500, // кол-во очков за ведьму
        witchFallPosition: 0, // место падения ведьмы
        hits: 0, // счетчик попаданий
        signals: {
            shot: new Phaser.Signal(), // просто выстрел
            endShot: new Phaser.Signal(), // конец выстрела
            hit: new Phaser.Signal(), // попал
            missed: new Phaser.Signal(), // промазал
            roundStart: new Phaser.Signal(),
            roundEnd: new Phaser.Signal(),
            preLevelStart: new Phaser.Signal(),
            levelStart: new Phaser.Signal(),
            levelEnd: new Phaser.Signal(),
            dogEnd: new Phaser.Signal(),
            flyAway: new Phaser.Signal(),
            gameOver: new Phaser.Signal(),
            toggleMusic: new Phaser.Signal()
        }
    };

    game.state.add('Boot', Boot);
    game.state.add('Preload', Preload);
    game.state.add('Intro', Intro);
    game.state.add('MainMenu', MainMenu);
    game.state.add('Game', Game);
    game.state.add('Result', Result);
    game.state.add('LeaderBoard', LeaderBoard);
    game.state.start('Boot');
})();
