Game = function (game) {
    this.flyAwayTimer = null;
    this.roundTimeout = 7000;
    this.flyAwayText = null;
    this.gameOverText = null;

    this.hud = null;
};
Game.prototype = {
    preload: function () {

    },
    create: function () {
        this.game.plugins.screenShake = this.game.plugins.add(Phaser.Plugin.ScreenShake);
        this.game.plugins.screenShake.setup({
            shakeX: false,
            shakeY: true
        });
        this.initEvents();
        // this.game.add.sprite(0, 0, 'background');
        // this.game.add.sprite(200, 170, 'tavern');
        this.background = new Background(this.game);
        this.witch = new Witch(this.game);
        // this.witch2 = new Witch(this.game);
        // this.boss = new Boss(this.game);
        this.frontBack = this.game.add.sprite(this.game.world.width, this.game.world.height, 'background_front');
        this.frontBack.anchor.set(1);
        this.dog = new Dog(this.game);
        this.frontFront = this.game.add.sprite(this.game.world.width, this.game.world.height, 'background_front');
        this.frontFront.anchor.set(1);
        this.frontFront.visible = false;
        this.shotgun = new Gun(this.game.world.centerX, 450, this.game);
        this.hud = new Hud(this.game, this.witch, this.dog, this.shotgun, this.background);
        this.game.global.signals.preLevelStart.dispatch();

        this.flyAwayText = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'flyAway');
        this.flyAwayText.anchor.set(.5);
        this.flyAwayText.visible = false;

        this.gameOverText = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'gameOver');
        this.gameOverText.anchor.set(.5);
        this.gameOverText.visible = false;

        this.musicIcon = this.game.add.button(this.game.world.leftX, this.game.world.topY, 'music', function(){
            this.game.global.signals.toggleMusic.dispatch();
        }, this);

        new BackgroundMusic(this.game);
    },
    update: function () {
    },
    initEvents: function () {
        this.game.global.signals.roundStart.add(function () {
            console.log('dispatch signal:', 'roundStart');
            clearTimeout(this.flyAwayTimer);
            this.hideFlyAway();
            if (this.game.global.level.isBoss) {
                // this.boss.reset(8);
            } else {
                this.flyAwayTimer = setTimeout(function () {
                    this.game.global.signals.flyAway.dispatch();
                    this.game.global.signals.roundEnd.dispatch();
                }.bind(this), this.roundTimeout);
                this.witch.reset(4 + this.game.global.level.number * 1.5);
            }
            this.frontFront.visible = true;
            this.game.global.bullets = 3;
            this.game.global.hits = 0;
            this.game.input.onDown.add(this.shotgun.shoot, this.shotgun);
        }, this);

        this.game.global.signals.roundEnd.add(function () {
            console.log('dispatch signal:', 'roundEnd');
            this.game.input.onDown.remove(this.shotgun.shoot, this.shotgun);
        }, this);

        this.game.global.signals.levelStart.add(function () {
            console.log('dispatch signal:', 'levelStart');
            // if (this.game.global.level.number == this.game.global.bossLevel) {
            //     this.game.global.level.isBoss = true;
            //     this.game.global.signals.roundStart.removeAll(this.witch);
            // }
            this.hideFlyAway();
            //todo: сделать нормально
            if (this.game.global.level.isBoss) {
                this.game.global.rounds = [];
                this.game.global.rounds.push({ witches: 10, killed: 0 });
            } else {
                this.game.global.rounds = [];
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
                this.game.global.rounds.push({ witches: 1, killed: 0 });
            }
            console.log('Level:', this.game.global.level.number);
            console.log('Round:', this.game.global.currentRound + 1);
            console.log('Witches: ', this.game.global.rounds);
        }, this);

        this.game.global.signals.levelEnd.add(function () {
            console.log('dispatch signal:', 'levelEnd');
            this.frontFront.visible = false;
        }, this);

        this.game.global.signals.dogEnd.add(function () {
            console.log('dispatch signal:', 'dogEnd');
            var levelIsEnd = this.game.global.currentRound == (this.game.global.rounds.length - 1);
            // var levelIsEnd = this.game.global.currentRound == 1;  //For debug
            if (levelIsEnd) {
                this.game.global.signals.levelEnd.dispatch();
                this.game.global.level.number++;
                this.game.global.currentRound = 0;
                var totalKilledWitchCount = 0;
                this.game.global.rounds.forEach(function (round) {
                    totalKilledWitchCount += round.killed;
                });
                if (totalKilledWitchCount >= this.game.global.level.minKilledWitches) {
                // if (totalKilledWitchCount >= 1) { //For debug
                    this.game.global.signals.preLevelStart.dispatch();
                } else {
                    this.game.global.signals.gameOver.dispatch();
                }
            } else {
                this.game.global.currentRound++;
                console.log('Round:', this.game.global.currentRound + 1);
                this.game.global.signals.roundStart.dispatch();
            }
        }, this);

        this.game.global.signals.gameOver.add(function () {
            console.log('dispatch signal:', 'gameOver');
            this.gameOverText.visible = true;
            setTimeout(function () {
                this.game.global.signals.shot.removeAll();
                this.game.global.signals.endShot.removeAll();
                this.game.global.signals.hit.removeAll();
                this.game.global.signals.missed.removeAll();
                this.game.global.signals.roundStart.removeAll();
                this.game.global.signals.roundEnd.removeAll();
                this.game.global.signals.levelStart.removeAll();
                this.game.global.signals.levelEnd.removeAll();
                this.game.global.signals.dogEnd.removeAll();
                this.game.global.signals.flyAway.removeAll();
                this.game.global.signals.gameOver.removeAll();
                this.game.global.signals.toggleMusic.removeAll();
                this.game.global.signals.preLevelStart.removeAll();
                this.state.start('Result');
            }.bind(this), 5000);
        }, this);

        this.game.global.signals.shot.add(function () {
            console.log('dispatch signal:', 'shot');
            this.game.global.bullets--;
            this.game.plugins.screenShake.shake(10); //camera shake
        }, this);

        this.game.global.signals.endShot.add(function () {
            console.log('dispatch signal:', 'endShot');
        }, this);

        this.game.global.signals.missed.add(function () {
            console.log('dispatch signal:', 'missed');
            var currentRoundWitches = this.game.global.rounds[this.game.global.currentRound];
            if (this.game.global.bullets == 0 && currentRoundWitches.killed < currentRoundWitches.witches) {
                clearTimeout(this.flyAwayTimer);
                this.game.global.signals.flyAway.dispatch();
                this.game.global.signals.roundEnd.dispatch();
            }
        }, this);

        this.game.global.signals.hit.add(function () {
            console.log('dispatch signal:', 'hit');
            this.game.global.rounds[this.game.global.currentRound].killed++;
            this.game.global.score += this.game.global.scorePerWitch;
            if (this.game.global.rounds[this.game.global.currentRound].witches == this.game.global.rounds[this.game.global.currentRound].killed){
                clearTimeout(this.flyAwayTimer);
            }
        }, this);

        this.game.global.signals.flyAway.add(function () {
            console.log('dispatch signal:', 'flyAway');
            this.showFlyAway();
        }, this);
    },
    showFlyAway: function () {
        if (this.flyAwayText) {
            this.flyAwayText.visible = true;
        }
    },
    hideFlyAway: function () {
        if (this.flyAwayText) {
            this.flyAwayText.visible = false;
        }
    }
};
