Intro = function (game) {
};
Intro.prototype = {
    preload: function () {
        this.stage.backgroundColor = '#000';
    },
    create: function () {
        this.logo = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'red_heat');
        this.logo.anchor.set(.5);
        this.logo.alpha = 0;
        this.logo.scale.set(.1);
        this.add.tween(this.logo.scale).to({x: 1.5, y: 1.5}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        var tween = this.add.tween(this.logo).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true, 0, 0, false);
        tween.onComplete.addOnce(function () {
            this.game.input.onDown.addOnce(this.gotoMainMenu, this);
        }, this);
        this.rhText = this.add.text(this.game.world.centerX, 440, "RED HEAT GAMES", {font: "17px pressFont", fill: "red"});
        this.rhText.anchor.set(.5);
        this.rhText.alpha = 0;
        this.add.tween(this.rhText).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true, 0, 0, false);

        this.clickText = this.add.text(this.game.world.centerX, 500, "CLICK MOUSE TO CONTINUE", {font: "12px pressFont", fill: "#C6C808"});
        this.clickText.anchor.set(.5);
        this.clickText.alpha = 0;
        this.add.tween(this.clickText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);
    },
    gotoMainMenu: function () {
        var tween = this.add.tween(this.logo).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        tween.onComplete.addOnce(function () {
            this.state.start('MainMenu');
        }, this);
        this.add.tween(this.rhText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        this.add.tween(this.clickText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
    },
    update: function () {

    }
};
