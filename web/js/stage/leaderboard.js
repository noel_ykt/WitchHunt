LeaderBoard = function (game) {
    this.users = [];
};
LeaderBoard.prototype = {
    preload: function () {

    },
    create: function () {
        var self = this;
        nanoajax.ajax({
            'method': 'GET',
            'url': '/api.php'
        }, function (code, responseText) {
            try {
                var jsonData = JSON.parse(responseText);
                self.users = jsonData;
                self.renderBoard();
            } catch (ex) {
                self.renderBoard();
            }
        });
    },
    renderBoard: function () {
        var coordY = 100;
        var textStyle = {
            font: "16px pressFont",
            fill: "#fff"
        };
        var self = this;
        this.users.forEach(function (user) {
            coordY = coordY + 30;
            self.add.text(self.game.world.centerX - 100, coordY, user.place + ': ' + user.name + ' - ' + user.score, textStyle);
        });
        this.hitText = this.add.text(this.game.world.centerX, 500, "PRESS \"ESC\" TO RETURN TO THE MENU", {font: "14px pressFont", fill: "#C6C808"});
        this.hitText.anchor.set(.5);
        this.hitText.alpha = 0;
        this.add.tween(this.hitText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);
        this.escKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    },
    update: function () {
        if (this.escKey && this.escKey.isDown) {
            this.goToMenu();
        }
    },
    goToMenu: function () {
        this.state.start('MainMenu');
    }
};