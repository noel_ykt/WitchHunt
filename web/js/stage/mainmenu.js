MainMenu = function (game) {
    this.enterKey = null
};
MainMenu.prototype = {
    create: function () {
        this.add.text(100, 100, 'WITCH', {font: "90px pressFont", fill: "#fff"});
        this.add.text(300, 200, 'HUNT', {font: "90px pressFont", fill: "#fff"});

        this.hitText = this.add.text(this.game.world.centerX, 400, "PRESS \"ENTER\" TO START GAME", {font: "17px pressFont", fill: "#C6C808"});
        this.hitText.anchor.set(.5);
        this.hitText.alpha = 0;
        this.add.tween(this.hitText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);

        this.lText = this.add.text(this.game.world.centerX, 440, "PRESS \"L\" TO SHOW LEADER BOARD", {font: "17px pressFont", fill: "#C6C808"});
        this.lText.anchor.set(.5);
        this.lText.alpha = 0;
        this.add.tween(this.lText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);

        this.enterKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.lKey = this.game.input.keyboard.addKey(Phaser.Keyboard.L);
    },
    startGame: function () {
        this.state.start('Game');
    },
    goToLBoard: function () {
        this.state.start('LeaderBoard');
    },
    update: function () {
        if (this.enterKey.isDown) {
            this.startGame();
        }
        if (this.lKey.isDown) {
            this.goToLBoard();
        }
    }
};