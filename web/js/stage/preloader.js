Preload = function (game) {

};
Preload.prototype = {
    preload: function () {
    },
    create: function () {
        this.text = this.add.text(this.game.world.centerX, this.game.world.centerY, 'Loading...', {fill: '#fff'});
        this.text.anchor.set(0.5);
        this.text.visible = false;

        this.load.onLoadStart.add(this.loadStart, this);
        this.load.onFileComplete.add(this.fileComplete, this);
        this.load.onLoadComplete.add(this.loadComplete, this);

        this.stage.backgroundColor = "#4488AA";
        this.load.image('background', 'assets/background.png');
        this.load.image('background_front', 'assets/frontBackground.png');
        this.load.image('sbutton', 'assets/sbutton.png');
        this.load.image('skull_red', 'assets/skull_red.png');
        this.load.image('skull_white', 'assets/skull_white.png');
        this.load.image('bar_red', 'assets/bar_red.png');
        this.load.image('bar_blue', 'assets/bar_blue.png');
        this.load.image('dialog_background', 'assets/dialog_background.png');
        this.load.image('cat_face', 'assets/cat_face.png');
        this.load.image('dog_face', 'assets/dog_face.png');
        this.load.image('witch_face', 'assets/witch_face.png');
        this.load.image('aim', 'assets/aim.png');
        this.load.image('flyAway', 'assets/flyAway.png');
        this.load.image('gameOver', 'assets/gameover.png');
        this.load.image('music', 'assets/music.png');
        this.load.image('red_heat', 'assets/RedHeat.png');
        this.load.image('blood', 'assets/blood.png');

        this.load.image('cat_face', 'assets/cat-face.png');
        this.load.image('tavern', 'assets/tavern.png');
        this.load.image('tavern_inside', 'assets/background_tavern_inside.png');
        this.load.image('cemetry', 'assets/background_cemetry.png');
        this.load.image('witch_face', 'assets/witch-face.png');

        this.load.spritesheet('shotgun', 'assets/shotgun.png', 150, 170, 6);
        this.load.spritesheet('bullets', 'assets/bullets.png', 47, 32, 4);
        this.load.spritesheet('witch_green', 'assets/witch/witch_green_97x95.png', 97, 95, 9);
        this.load.spritesheet('witch_blue', 'assets/witch/witch_blue_97x95.png', 97, 95, 9);
        this.load.spritesheet('witch_red', 'assets/witch/witch_red_97x95.png', 97, 95, 9);
        this.load.spritesheet('dog', 'assets/dog.png', 60, 60, 13);
        this.load.spritesheet('boss_right', 'assets/boss/right2.png', 150, 150, 8);
        this.load.spritesheet('boss_left', 'assets/boss/left2.png', 150, 150, 8);

        this.load.audio('shotgun', 'assets/audio/shotgun.mp3');
        this.load.audio('witchLaugh', 'assets/audio/witchlaugh.mp3');
        this.load.audio('dogLaugh', 'assets/audio/dog-laugh.mp3');
        this.load.audio('witchFalls', 'assets/audio/duck-falls.mp3');
        this.load.audio('witchLands', 'assets/audio/duck-lands.mp3');
        this.load.audio('bgmusic1', 'assets/audio/music/lonely-witch.mp3');
        this.load.audio('bgmusic2', 'assets/audio/music/this-is-halloween.mp3');
        this.load.audio('bgmusic3', 'assets/audio/music/dark-chest-of-wonders.mp3');

        this.load.start();
    },
    update: function () {

    },
    loadStart: function () {
        this.text.visible = true;
    },
    fileComplete: function (progress, cacheKey, success, totalLoaded, totalFiles) {
        this.text.setText("Loading " + progress + "% - " + totalLoaded + " out of " + totalFiles);
    },
    loadComplete: function () {
        this.state.start('Intro');
    }
};
