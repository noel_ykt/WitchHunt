Result = function (game) {
};
Result.prototype = {
    preload: function () {
        this.add.plugin(Fabrique.Plugins.InputField);
    },
    create: function () {
        var textStyle = {
            font: "16px pressFont",
            fill: "#fff"
        };
        var scoreText = this.add.text(this.game.world.centerX, this.game.world.centerY - 100, 'Вы выиграли: ' + this.game.global.score, textStyle);
        scoreText.anchor.set(.5);

        this.sendText = this.add.text(this.game.world.centerX, 400, "Сохранить результат", {font: "14px pressFont", fill: "#C6C808"});
        this.sendText.anchor.set(.5);
        this.sendText.inputEnabled = true;
        this.sendText.input.useHandCursor = true;
        this.sendText.events.onInputDown.add(this.sendUsername, this);

        this.input = this.add.inputField(this.game.world.centerX - 150, this.game.world.centerY - 68, {
            font: '16px pressFont',
            fill: '#999',
            fontWeight: 'bold',
            width: 290,
            height: 20,
            padding: 8,
            backgroundColor: '#000',
            borderWidth: 0,
            placeHolder: 'Введите свое имя...',
            cursorColor: '#ccc'
        });
        this.input.setText(localStorage.getItem('username') || '');
    },
    update: function () {
        this.input.update();
    },
    sendUsername: function () {
        var username = this.input.value;
        if (username.trim().length > 0) {
            var self = this;
            var url = '/api.php?name=' + username + '&score=' + this.game.global.score;
            localStorage.setItem('username', username);
            nanoajax.ajax({
                'method': 'GET',
                'url': url
            }, function (code, responseText) {
                self.game.global.score = 0;
                self.goToLBoard();
            });
        }
    },
    goToLBoard: function () {
        this.state.start('LeaderBoard');
    }
};